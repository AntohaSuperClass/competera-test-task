from armani.items import Product
from TestObj import TestObj
from TestRegionObj import TestingRegion
import csv
import ast

def prodConstructor(row):
    p = Product()
    p["name"] = row["name"]
    p["price"] = ast.literal_eval(row["price"])
    p["currency"] = row["currency"]
    p["time"] = ast.literal_eval(row["time"])
    p["color"] = row["color"].split(",")
    p["size"] = row["size"].split(",")
    p["region"] = row["region"]
    p["prodType"] = row["prodType"]
    p["description"] = ast.literal_eval(row["description"])
    return p

allProds = []
usProds = []
frProds = []
with open("data.csv","r") as file:
    reader = csv.DictReader(file)
    for r in reader:
        prod = prodConstructor(r)
        allProds.append(prod)
        if prod["region"] == "us":
            usProds.append(prod)
        else:
            frProds.append(prod)
    file.close()
allTest = TestObj(allProds)
usTest = TestingRegion(usProds,"us")
frTest = TestingRegion(frProds,"fr")
allTest.test()
usTest.test()
frTest.test()
allTest.JSON_Serialize("Test_Result_ALL.json")
usTest.JSON_Serialize("Test_Result_US.json")
frTest.JSON_Serialize("Test_Result_FR.json")
print(allTest)
print(usTest)
print(frTest)