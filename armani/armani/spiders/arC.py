import scrapy
import re
import timeit
from armani.items import Product
import os

class ArmaniSpyder(scrapy.Spider):
    name = "as"
    allowed_domains = ["www.armani.com"]
    items = []
    regs = []
    h = {
        "Host": "www.armani.com",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "Accept-Language": "uk-UA,uk;q=0.8,ru;q=0.6,en-US;q=0.4,en;q=0.2",
        "Cache-Control": "max-age=0",
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36 OPR/46.0.2597.57"
    }
    def start_requests(self):
        try:
            for r in self.r.split(","):
                    self.regs.append(r.lower())
        except Exception:
                self.regs = ["us","fr"]
        for u in self.regs:
            print("Searching regions is: " + u)
            yield scrapy.http.Request("http://" + self.allowed_domains[0] + "/" + u,callback=self.parse,dont_filter=True,headers=self.h)

    def parse(self,response):
        w = response.xpath("//*[@id=\"menuBar\"]/div/div/div/ul/li[1]/a/text()").extract()[0]
        m = response.xpath("//*[@id=\"menuBar\"]/div/div/div/ul/li[2]/a/text()").extract()[0]
        if m == "Man":
            m=m.replace("a","e")
        if w == "Woman":
            w = w.replace("a","e")
        refs = self.createURLs([w,m],response.url.split("/")[-1])
        for a in refs:
            yield scrapy.http.Request(a,callback=self.contentParse,headers=self.h)

    def unescape(self,s):
        s = re.sub("[^\x00-\x7f]","",s)
        return s.encode("utf8").replace("\t","").replace("\r","").replace("\n","")

    def createURLs(self,genders,reg):
        refs = []
        readyRefs = []
        with open(os.getcwd()+ "/armani/spiders/URLs.txt","r") as f:
            refs = f.readlines()
            f.close()
        for i in range(len(refs)):
            refs[i] = refs[i].replace("<REGION>",reg)
        for g in genders:
            for r in refs:
                readyRefs.append(r.replace("<GENDER>",g).replace("\n","").encode("utf8"))
        return readyRefs

    def contentParse(self,response):\
        for r in response.xpath("//*[@id=\"elementsContainer\"]/div/a/@href").extract():
            yield scrapy.http.Request("http://{0}{1}".format(self.allowed_domains[0],r.encode("utf8")),callback=self.itemParse,headers=self.h)

    def itemParse(self,response):
        item = Product()
        t = timeit.default_timer()
        # getting name
        item["name"] = self.unescape(response.xpath("//*[@id=\"pageContent\"]/article/aside/h1//text()").extract()[0])
        # getting color
        colors = []
        for color in response.xpath("//*[@class=\"Colors\"]//a/text()").extract():
            colors.append(self.unescape(color))
        item["color"] = colors
        # getting size
        sizes = []
        for size in response.xpath("//*[@class=\"SizeW\"]//a/text()").extract():
            sizes.append(self.unescape(size))
        item["size"] = sizes
        # getting currency
        if response.xpath("//*[@class=\"currency\"]/text()").extract()[0] == "$":
            item["currency"] = "USD"
        else:
            item["currency"] = "EUR"
        # getting price
        item["price"] = float(response.xpath("//*[@class=\"priceValue\"]/text()").extract()[0].replace(",",""))
        # getting description
        description = {}
        ats = response.xpath("//*[@class=\"articleName\"]//span/node()").extract()
        description.update({self.unescape(ats[0]).replace(":",""): self.unescape(ats[1])})
        if response.xpath("//*[@class=\"attributes\"]/text()").extract() != []:
            ats = response.xpath("//*[@class=\"attributes\"]/node()").extract()
            for i in range(0,len(ats),3):
                description.update({self.unescape(ats[i]).replace("<span>","").replace("</span>","").replace(":","") : self.unescape(ats[i+1]) })
        detail = ""
        for d in response.xpath("//*[@class=\"descriptionList\"]//li/text()").extract():
            detail += self.unescape(d) + "\n"
        detail += self.unescape(response.xpath("//*[@class=\"info madeIn\"]/text()").extract()[0])
        description.update({"detail": detail})
        item["description"] = description
        # getting product type
        item["prodType"] = re.findall("[- a-zA-Z0-9]+_",response.url.split("/")[-1])[0].replace("_","")
        # getting region
        item["region"] = response.url.split("/")[-3]
        # getting time
        item["time"] = timeit.default_timer() - t
        if not item in self.items:
            self.items.append(item)
            return item
    
