# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class Product(scrapy.Item):
    name = scrapy.Field()
    price = scrapy.Field()
    currency = scrapy.Field()
    time = scrapy.Field()
    color = scrapy.Field()
    size = scrapy.Field()
    region = scrapy.Field()
    description = scrapy.Field()
    prodType = scrapy.Field()
    
    def __eq__(self,other):
        if (self["name"] == other["name"])and(self["price"] == other["price"])and(self["currency"] == other["currency"])and(self["color"] == other["color"])and(self["size"] == other["size"])and(self["region"] == other["region"])and(self["description"] == other["description"])and(self["prodType"] == other["prodType"]):
            return True
        else:
            return False

