from armani.items import Product
from TestObj import TestObj

class TestingRegion(TestObj):
    reg = ""
    currency = ""
    currencyState = ""
    def __init__(self,items,reg):
        TestObj.__init__(self,items)
        self.reg = reg
        if reg == "us":
            self.currensy = "USD"
            self.currencyState = self.testCurrency("USD") 
        else:
            self.currensy = "EUR"
            self.currencyState = self.testCurrency("EUR") 

    def __str__(self):
        return TestObj.__str__(self) + """
CurrencyState: {0}""".format(self.currencyState)

    def testCurrency(self,curr):
        for i in self.items:
            if i["currency"] != curr:
                print(self.reg,self.currency,i["currency"])
                return "Region have currency mistakes!"
        return "Currency in {0} region is alright!".format(self.reg.upper())