from armani.items import Product
import json

class TestObj:
    items = []
    itemsCount = 0
    # color
    oneColorCount = 0
    oneColorPercent = ""
    nonOneColorCount = 0
    nonOneColorPercent = ""
    # size
    oneSizeCount = 0
    oneSizePercent = ""
    nonOneSizeCount = 0
    nonOneSizePercent = ""
    # attribute
    attrCount = 0
    attrPercent = ""
    nonAttrCount = 0
    nonAttrPercent = ""
    # description
    descriptionCount = 0
    descriptionPercent = ""
    nonDescriptionCount = 0
    nonDescriptionPercent = ""
    # time
    averageScanTime = 0

    def __init__(self,items):
        self.items = items
        self.itemsCount = len(self.items)

    def __str__(self):
        return """
TESTING RESULT:
ItemsCount:{0}
Items with one color:{1}
Items with one color(in percentages):{2}
Items with more than one color:{3}
Items with more than one color(in percentages):{4}
Items with one size:{5}
Items with one size(in percentages):{6}
Items with more than one size:{7}
Items with more than one size(in percentages):{8}
Items with attributes:{9}
Items with attributes(in percentages):{10}
Items that dont have attributes:{11}
Items that dont have attributes(in percentages):{12}
Items with description:{13}
Items with description(in percentages):{14}
Items that dont have description:{15}
Items that dont have description(in percentages):{16}
Item average scan time:{17}""".format(self.itemsCount,self.oneColorCount,self.oneColorPercent,self.nonOneColorCount,self.nonOneColorPercent,self.oneSizeCount,self.oneSizePercent,self.nonOneSizeCount,self.nonOneSizePercent,self.attrCount,self.attrPercent,self.nonAttrCount,self.nonAttrPercent,self.descriptionCount,self.descriptionPercent,self.nonDescriptionCount,self.nonDescriptionPercent,self.averageScanTime)

    def testColor(self,item):
        colors = item["color"]
        if len(colors) == 1:
            self.oneColorCount += 1
        else:
            self.nonOneColorCount +=1
    
    def testSize(self,item):
        sizes = item["size"]
        if len(sizes) == 1:
            self.oneSizeCount += 1
        else:
            self.nonOneSizeCount +=1

    def testAttribute(self,item):
        if len(item["description"]) != 2:
            self.attrCount += 1
        else:
            self.nonAttrCount += 1

    def testDescription(self,item):
        if item["description"]["detail"]:
            self.descriptionCount += 1
        else:
            self.nonDescriptionCount += 1


    def test(self):
        s = 0
        for i in self.items:
            self.testColor(i)
            self.testSize(i)
            self.testAttribute(i)
            self.testDescription(i)
            s += i["time"]
        oneItemValue = 100.0/self.itemsCount
        self.oneColorPercent = "{0}%".format(oneItemValue*self.oneColorCount)
        self.nonOneColorPercent = "{0}%".format(oneItemValue*self.nonOneColorCount)
        self.oneSizePercent = "{0}%".format(oneItemValue*self.oneSizeCount)
        self.nonOneSizePercent = "{0}%".format(oneItemValue*self.nonOneSizeCount)
        self.attrPercent = "{0}%".format(oneItemValue*self.attrCount)
        self.nonAttrPercent = "{0}%".format(oneItemValue*self.nonAttrCount)
        self.descriptionPercent = "{0}%".format(oneItemValue*self.descriptionCount)
        self.nonDescriptionPercent = "{0}%".format(oneItemValue*self.nonDescriptionCount)
        self.averageScanTime = s/self.itemsCount

    def JSON_Serialize(self,path = ""):
        temp = self.items
        self.items = str(type(self.items))
        fs = ""
        if path:
            fs = path
        else:
            fs ="test_Result.json"
        with open(fs,"w") as file:
                file.writelines(json.dumps(self,default=lambda o: o.__dict__,sort_keys=True,indent=4))
                file.close()
        self.items = temp